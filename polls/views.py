from django.shortcuts import get_object_or_404
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED

from .models import Poll
from .serializers import PollSerializer
from .helpers import ResponseHelper


class PollViewSet(CreateModelMixin, ListModelMixin, RetrieveModelMixin, GenericViewSet):
    queryset = Poll.objects.all()
    serializer_class = PollSerializer
    lookup_field = 'uuid'

    @action(detail=True, methods=['POST'])
    def vote(self, request, uuid=None):
        poll = get_object_or_404(Poll.objects.prefetch_related('fields'), uuid=uuid)
        validator = ResponseHelper(poll.fields.all(), request, poll)
        validator.validate()
        validator.save()
        return Response(status=HTTP_201_CREATED)
