from rest_framework.serializers import ValidationError
from ipware import get_client_ip
from .models import TextFieldElement, SelectElement, CheckboxElement, MultipleSelectElement, RadioButtonElement, \
    IntegerPickerElement, CharResponse, BoolResponse, IntegerResponse, ArrayResponse, VotedBy
from .serializers import CharResponseSerializer, BoolResponseSerializer, ArrayResponseSerializer, \
    IntegerResponseSerializer, ElementField


class ResponseHelper:

    def __init__(self, fields, request, poll):
        try:
            self.fields = {field.element_order: self.convert_to_child(field) for field in fields}
            self.responses = {response['element_order']: response['response'] for response in request.data}
            self.user = request.user
            self.poll = poll
            self.data = self.get_request_info(request)
        except Exception:
            raise ValidationError('Wrong params')

    def convert_to_child(self, instance):
        if instance.type == 'TextField':
            obj = TextFieldElement.objects.get(pk=instance.pk)
        elif instance.type == 'Select':
            obj = SelectElement.objects.get(pk=instance.pk)
        elif instance.type == 'MultipleSelect':
            obj = MultipleSelectElement.objects.get(pk=instance.pk)
        elif instance.type == 'RadioButton':
            obj = RadioButtonElement.objects.get(pk=instance.pk)
        elif instance.type == 'Checkbox':
            obj = CheckboxElement.objects.get(pk=instance.pk)
        else:
            obj = IntegerPickerElement.objects.get(pk=instance.pk)
        return obj

    @staticmethod
    def get_request_info(request):
        ip, _ = get_client_ip(request)
        data = {
            'ip': ip,
            'user_agent': request.META['HTTP_USER_AGENT']
        }
        return data

    def validate(self):
        self.validate_access()
        self.validate_already_voted()
        self.validate_data()

    def validate_already_voted(self):
        if not self.user.is_anonymous and VotedBy.objects.filter(user=self.user, poll=self.poll).exists():
            raise ValidationError("You are already voted in this poll")
        if self.user.is_anonymous and \
                VotedBy.objects.filter(user_data__ip=self.data.get('ip'), poll=self.poll).exists():
            raise ValidationError("You are already voted in this poll")

    def validate_access(self):
        if self.poll.access_level == 'OV' and self.user.is_anonymous:
            raise ValidationError("You don't have permissions to vote this poll")
        if self.poll.access_level == 'OV' and not self.user.email_verified:
            raise ValidationError("You don't have permissions to vote this poll")
        elif self.poll.access_level == 'AU' and self.user.is_anonymous:
            raise ValidationError("You don't have permissions to vote this poll")

    def validate_data(self):
        for key in self.responses.keys():
            field = self.fields[key]
            if field.type == 'TextField':
                self.validate_text(self.responses[key])
            elif field.type in ['Select', 'RadioButton']:
                self.validate_select_radio(key, self.responses[key])
            elif field.type == 'MultipleSelect':
                self.validate_multiple(key, self.responses[key])
            elif field.type == 'Checkbox':
                self.validate_checkbox(self.responses[key])
            else:
                self.validate_integer(key, self.responses[key])

    def validate_text(self, value):
        if not isinstance(value, str):
            raise ValidationError('Wrong response for text field')

    def validate_select_radio(self, key, value):
        self.validate_text(value)
        if value not in self.fields[key].all_values:
            raise ValidationError("Value must be part of proposed choices")

    def validate_multiple(self, key, values):
        [self.validate_text(value) for value in values]
        if not all(value in self.fields[key].all_values for value in values):
            raise ValidationError("Values must be part of proposed choices")

    def validate_checkbox(self, value):
        if not isinstance(value, bool):
            raise ValidationError('Wrong response for checkbox field')

    def validate_integer(self, key, value):
        if not isinstance(value, int):
            raise ValidationError('Wrong response for integer field')
        if not self.fields[key].min_value <= value <= self.fields[key].max_value:
            raise ValidationError('Response should be in field range')

    def save(self):
        for key, field in self.fields.items():
            if field.type in ['TextField', 'Select', 'RadioButton']:
                model = CharResponse
                response_type = 'CharResponse'
            elif field.type == 'MultipleSelect':
                model = ArrayResponse
                response_type = 'ArrayResponse'
            elif field.type == 'Checkbox':
                model = BoolResponse
                response_type = 'BoolResponse'
            else:
                model = IntegerResponse
                response_type = 'IntegerResponse'
            model.objects.create(element=field,
                                 response=self.responses.get(key, field.default_value),
                                 type=response_type)
            VotedBy.objects.create(poll=self.poll,
                                   user=self.user if not self.user.is_anonymous else None,
                                   user_data=self.data)
