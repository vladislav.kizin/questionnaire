import uuid
from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from django.utils.translation import gettext_lazy as _

USER_MODEL = get_user_model()


class Poll(models.Model):
    """
    Poll class
    Fields:
        - uuid - 16 symbols, randomly generated
        - description - char/field
        - fields: foreign key on elements
        - access_level - default with login
        - created - flag to tag poll as completed
        - creator - user, that created poll
    """
    class AccessLevel(models.TextChoices):
        AUTHORIZED = 'AU', _('Authorized')
        ALLOW_ANY = 'AA', _('Allow Any')
        ONLY_VERIFIED = 'OV', _('Only verified')

    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    description = models.TextField()
    access_level = models.CharField(max_length=2, choices=AccessLevel.choices)
    created = models.BooleanField(default=False)
    creator = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE, related_name='polls', null=True)
    voted_by = models.ManyToManyField(USER_MODEL, related_name='voted_in', through='VotedBy')


class VotedBy(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    user = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE, null=True)
    user_data = models.JSONField()


class BaseElement(models.Model):
    """
    Base element class
    Fields:
        - text before element - label
        - text after element - can be none
        - element order number
        - poll - foreign key to poll
    """
    text_before = models.CharField(max_length=280)
    text_after = models.CharField(max_length=100, blank=True)
    element_order = models.PositiveIntegerField()
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE, related_name='fields')
    type = models.CharField(max_length=40)


class TextFieldElement(BaseElement):
    """
    Text field class - inherit Base
    Fields:
        - Default value
        - Element type
    """
    default_value = models.CharField(max_length=280)

    @property
    def type(self):
        return 'TextField'

    @type.setter
    def type(self, value):
        if value != 'TextField':
            raise ValidationError('Fail')


class SelectElement(BaseElement):
    """
    Select class - inherit Base
    Fields:
        - All_values - array field
        - Default value - char field
        - Element type
    """
    all_values = ArrayField(models.CharField(max_length=100))
    default_value = models.CharField(max_length=100)

    @property
    def type(self):
        return 'Select'

    @type.setter
    def type(self, value):
        if value != 'Select':
            raise ValidationError('Fail')


class MultipleSelectElement(BaseElement):
    """
    Multiple select class - inherit Base
    Fields:
        - Default value - boolean field
        - Element type
    """
    all_values = ArrayField(models.CharField(max_length=100))
    default_value = ArrayField(models.CharField(max_length=100))

    @property
    def type(self):
        return 'MultipleSelect'

    @type.setter
    def type(self, value):
        if value != 'MultipleSelect':
            raise ValidationError('Fail')


class RadioButtonElement(SelectElement):
    """
    Radio button class - inherit SelectElement
    Fields:
        - Element type
    """

    @property
    def type(self):
        return 'RadioButton'

    @type.setter
    def type(self, value):
        if value != 'RadioButton':
            raise ValidationError('Fail')


class CheckboxElement(BaseElement):
    """
    Checkbox class - inherit Base
    Fields:
        - Default value - boolean field
        - Element type
    """
    default_value = models.BooleanField()

    @property
    def type(self):
        return 'Checkbox'

    @type.setter
    def type(self, value):
        if value != 'Checkbox':
            raise ValidationError('Fail')


class IntegerPickerElement(BaseElement):
    """
    Integer picker - inherit Base
    Fields:
        - max - can be none
        - min - can be none
        - default value
        - Element type
    """
    min_value = models.IntegerField()
    max_value = models.IntegerField()
    default_value = models.IntegerField()

    @property
    def type(self):
        return 'IntegerPicker'

    @type.setter
    def type(self, value):
        if value != 'IntegerPicker':
            raise ValidationError('Fail')


class Response(models.Model):
    """
    Response class to extend other responses if needed
    """
    type = models.CharField(max_length=40)
    element = models.ForeignKey(BaseElement, on_delete=models.CASCADE, related_name='responses')


class CharResponse(Response):
    """
    Char response
    """
    response = models.CharField(max_length=280)

    @property
    def type(self):
        return 'CharResponse'

    @type.setter
    def type(self, value):
        if value != 'CharResponse':
            raise ValidationError('Fail')


class BoolResponse(Response):
    """
    Char response
    """
    response = models.BooleanField()

    @property
    def type(self):
        return 'BoolResponse'

    @type.setter
    def type(self, value):
        if value != 'BoolResponse':
            raise ValidationError('Fail')


class IntegerResponse(Response):
    """
    Integer response
    """
    response = models.IntegerField()

    @property
    def type(self):
        return 'IntegerResponse'

    @type.setter
    def type(self, value):
        if value != 'IntegerResponse':
            raise ValidationError('Fail')


class ArrayResponse(Response):
    """
    Array response
    """
    response = ArrayField(models.CharField(max_length=100))

    @property
    def type(self):
        return 'ArrayResponse'

    @type.setter
    def type(self, value):
        if value != 'ArrayResponse':
            raise ValidationError('Fail')
