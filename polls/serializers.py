from rest_framework import serializers

from .models import Poll, BaseElement, TextFieldElement, RadioButtonElement, SelectElement, MultipleSelectElement, \
    CheckboxElement, IntegerPickerElement, CharResponse, BoolResponse, IntegerResponse, ArrayResponse


class CurrentUserDefault:
    requires_context = True

    def __call__(self, serializer_field):
        user = serializer_field.context['request'].user
        return user if not user.is_anonymous else None

    def __repr__(self):
        return '%s()' % self.__class__.__name__


class CharResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = CharResponse
        fields = ['response']


class BoolResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = BoolResponse
        fields = ['response']


class IntegerResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = IntegerResponse
        fields = ['response']


class ArrayResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = ArrayResponse
        fields = ['response']

    response = serializers.ListField(child=serializers.CharField())


class ResponseField(serializers.BaseSerializer):

    def to_representation(self, instance):
        if isinstance(instance, CharResponse):
            serializer = CharResponseSerializer(data=instance)
        elif isinstance(instance, BoolResponse):
            serializer = BoolResponseSerializer(data=instance)
        elif isinstance(instance, IntegerResponse):
            serializer = IntegerResponseSerializer(data=instance)
        elif isinstance(instance, ArrayResponse):
            serializer = ArrayResponseSerializer(data=instance)
        else:
            return self.get_nested(instance)
        serializer.is_valid(raise_exception=True)
        return serializer.data

    def to_internal_value(self, data):
        return data

    def get_nested(self, instance):
        if instance.type == 'CharResponse':
            obj = CharResponse.objects.get(pk=instance.pk)
            return CharResponseSerializer(instance=obj).data
        elif instance.type == 'BoolResponse':
            obj = BoolResponse.objects.get(pk=instance.pk)
            return BoolResponseSerializer(instance=obj).data
        elif instance.type == 'IntegerResponse':
            obj = IntegerResponse.objects.get(pk=instance.pk)
            return IntegerResponseSerializer(instance=obj).data
        else:
            obj = ArrayResponse.objects.get(pk=instance.pk)
            return ArrayResponseSerializer(instance=obj).data


class BaseElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseElement
        fields = ['text_before', 'text_after', 'element_order', 'responses']

    responses = ResponseField(many=True, required=False)


class TextFieldElementSerializer(BaseElementSerializer):
    class Meta:
        model = TextFieldElement
        fields = BaseElementSerializer.Meta.fields + ['type', 'default_value']


class SelectElementSerializer(BaseElementSerializer):
    class Meta:
        model = SelectElement
        fields = BaseElementSerializer.Meta.fields + ['type', 'default_value', 'all_values']

    all_values = serializers.ListField(child=serializers.CharField())

    def validate(self, attrs):
        if not attrs.get('default_value') in attrs.get('all_values'):
            raise serializers.ValidationError('Default choice must be in the all choices')
        return attrs


class MultipleSelectElementSerializer(BaseElementSerializer):
    class Meta:
        model = MultipleSelectElement
        fields = BaseElementSerializer.Meta.fields + ['type', 'default_value', 'all_values']

    all_values = serializers.ListField(child=serializers.CharField())
    default_value = serializers.ListField(child=serializers.CharField())

    def validate(self, attrs):
        if not all(default in attrs.get('all_values') for default in attrs.get('default_value')):
            raise serializers.ValidationError('All default choices must be in the all choices')


class RadioButtonElementSerializer(SelectElementSerializer):
    class Meta:
        model = RadioButtonElement
        fields = SelectElementSerializer.Meta.fields


class CheckboxElementSerializer(BaseElementSerializer):
    class Meta:
        model = CheckboxElement
        fields = BaseElementSerializer.Meta.fields + ['type', 'default_value']


class IntegerPickerElementSerializer(BaseElementSerializer):
    class Meta:
        model = IntegerPickerElement
        fields = BaseElementSerializer.Meta.fields + ['type', 'default_value', 'min_value', 'max_value']

    def validate(self, attrs):
        if not attrs.get('min_value') < attrs.get('default_value') < attrs.get('max_value'):
            raise serializers.ValidationError('Default value should be in selected min-max range')
        return attrs


class ElementField(serializers.BaseSerializer):

    def to_representation(self, instance):
        if isinstance(instance, TextFieldElement):
            serializer = TextFieldElementSerializer(data=instance)
        elif isinstance(instance, RadioButtonElement):
            serializer = RadioButtonElementSerializer(data=instance)
        elif isinstance(instance, SelectElement):
            serializer = SelectElementSerializer(data=instance)
        elif isinstance(instance, MultipleSelectElement):
            serializer = MultipleSelectElementSerializer(data=instance)
        elif isinstance(instance, CheckboxElement):
            serializer = CheckboxElementSerializer(data=instance)
        elif isinstance(instance, IntegerPickerElement):
            serializer = IntegerPickerElementSerializer(data=instance)
        else:
            return self.get_nested(instance)
        serializer.is_valid(raise_exception=True)
        return serializer.data

    def to_internal_value(self, data):
        return data

    def get_nested(self, instance):
        if instance.type == 'TextField':
            obj = TextFieldElement.objects.get(pk=instance.pk)
            return TextFieldElementSerializer(instance=obj).data
        elif instance.type == 'Select':
            obj = SelectElement.objects.get(pk=instance.pk)
            return SelectElementSerializer(instance=obj).data
        elif instance.type == 'MultipleSelect':
            obj = MultipleSelectElement.objects.get(pk=instance.pk)
            return MultipleSelectElementSerializer(instance=obj).data
        elif instance.type == 'RadioButton':
            obj = RadioButtonElement.objects.get(pk=instance.pk)
            return RadioButtonElementSerializer(instance=obj).data
        elif instance.type == 'Checkbox':
            obj = CheckboxElement.objects.get(pk=instance.pk)
            return CheckboxElementSerializer(instance=obj).data
        else:
            obj = IntegerPickerElement.objects.get(pk=instance.pk)
            return IntegerPickerElementSerializer(instance=obj).data


# class ElementWithResponse(ElementField):
#     def to_representation(self, instance):
#         data = super().to_representation(instance)
#         if isinstance(instance, Response):
#             serializer = ResponseField(many=True)
#             serializer.is_valid(raise_exception=True)
#             return data.update({'response': serializer.data})
#         else:
#             return data.update({'response': []})
#
#     def create(self, validated_data):
#         print(validated_data)
#         return {}


class PollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
        fields = ['url', 'description', 'access_level', 'creator', 'fields']

    url = serializers.HyperlinkedIdentityField(view_name='polls-detail', lookup_field='uuid')
    access_level = serializers.ChoiceField(choices=Poll.AccessLevel)
    creator = serializers.HiddenField(default=CurrentUserDefault())
    fields = ElementField(many=True)

    def create(self, validated_data):
        fields = validated_data.pop('fields')
        poll = Poll.objects.create(created=True, **validated_data)
        for field in fields:
            if field.get('type') == 'TextField':
                TextFieldElement.objects.create(poll=poll, **field)
            elif field.get('type') == 'Select':
                SelectElement.objects.create(poll=poll, **field)
            elif field.get('type') == 'MultipleSelect':
                MultipleSelectElement.objects.create(poll=poll, **field)
            elif field.get('type') == 'RadioButton':
                RadioButtonElement.objects.create(poll=poll, **field)
            elif field.get('type') == 'Checkbox':
                CheckboxElement.objects.create(poll=poll, **field)
            else:
                IntegerPickerElement.objects.create(poll=poll, **field)
        return poll

    def validate(self, attrs):
        order_numbers = [field['element_order'] for field in attrs.get('fields')]
        if len(order_numbers) != len(set(order_numbers)):
            raise serializers.ValidationError('Order numbers must be unique')
        return attrs
