# Generated by Django 3.1.3 on 2020-11-09 08:24

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('polls', '0002_auto_20201106_1335'),
    ]

    operations = [
        migrations.CreateModel(
            name='VotedBy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_data', models.JSONField()),
                ('poll', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.poll')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.DeleteModel(
            name='RadioButtonElement',
        ),
        migrations.AddField(
            model_name='poll',
            name='voted_by',
            field=models.ManyToManyField(related_name='voted_in', through='polls.VotedBy', to=settings.AUTH_USER_MODEL),
        ),
    ]
