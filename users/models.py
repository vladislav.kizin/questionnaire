import asyncio
import os
from uuid import uuid4
from django.core.cache import cache
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.shortcuts import reverse


class User(AbstractUser):

    email = models.EmailField(unique=True)
    email_verified = models.BooleanField(default=False)

    def __str__(self):
        return self.username

    def save(self, *args, **kwargs):
        if not self.pk:
            self.verify_email()
        super().save(*args, **kwargs)

    def verify_email(self):
        code = str(uuid4())
        cache.set(code, self.email, timeout=86400)
        try:
            loop = asyncio.get_event_loop()
        except Exception:
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
        try:
            loop.run_in_executor(None,
                                 self.email_user,
                                 'Email verification',
                                 f'{os.environ.get("SITE_DOMAIN", "http://127.0.0.1:8000")}{reverse("users-list")}'
                                 f'activate/{code}/'
                                 )
        except Exception:
            loop.close()
