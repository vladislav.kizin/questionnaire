from django.urls import path, include
from rest_framework.routers import SimpleRouter
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)

from users.urls import urlpatterns as users_urls

from .views import PollViewSet

router = SimpleRouter()
router.register('polls', PollViewSet, basename='polls')

urlpatterns = [
    path('users/', include(users_urls)),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('', include(router.urls))
]
