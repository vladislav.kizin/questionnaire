import uuid
from django.urls import reverse, include, path
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase
from unittest import mock
from django.core.cache import cache

from .models import User
from .urls import urlpatterns as users_urls


def fake_verify_email(self):
    cache.set('test', 'testuser@gmail.com')


class UserTests(APITestCase, URLPatternsTestCase):
    urlpatterns = [
        path('api/', include(users_urls)),
    ]

    def test_create_account(self):
        url = reverse('users-list')
        data = {'username': 'TestUser', 'email': 'testuser@gmail.com', 'password': 'TestPassword'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)

    def test_create_account_without_password(self):
        url = reverse('users-list')
        data = {'username': 'WithoutPassword'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.filter(username='WithoutPassword').count(), 0)

    def test_create_account_without_username(self):
        url = reverse('users-list')
        data = {'password': 'TestPassword'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_account_without_email(self):
        url = reverse('users-list')
        data = {'email': 'testuser2@gmail.com'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_unique_email(self):
        url = reverse('users-list')
        data = {'username': 'TestUser', 'email': 'testuser3@gmail.com', 'password': 'TestPassword'}
        data2 = {'username': 'TestUser', 'email': 'testuser3@gmail.com', 'password': 'TestPassword'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.post(url, data2, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @mock.patch('users.models.User.verify_email', new=fake_verify_email)
    def test_activation_on_create(self):
        url = reverse('users-list')
        data = {'username': 'TestUser', 'email': 'testuser@gmail.com', 'password': 'TestPassword'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        url = reverse('users-activate', kwargs={'uuid': 'test'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(User.objects.get(username='TestUser').email_verified)

    @mock.patch('users.models.User.verify_email', new=fake_verify_email)
    def test_activation_on_create_fail(self):
        url = reverse('users-list')
        data = {'username': 'TestUser', 'email': 'testuser@gmail.com', 'password': 'TestPassword'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        url = reverse('users-activate', kwargs={'uuid': '1111'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertFalse(User.objects.get(username='TestUser').email_verified)

    @mock.patch('users.models.User.verify_email', new=fake_verify_email)
    def test_activation_again(self):
        url = reverse('users-list')
        data = {'username': 'TestUser', 'email': 'testuser@gmail.com', 'password': 'TestPassword'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        cache.delete('test')
        url = reverse('users-email')
        data = {'email': 'testuser@gmail.com'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        url = reverse('users-activate', kwargs={'uuid': 'test'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(User.objects.get(username='TestUser').email_verified)

    @mock.patch('users.models.User.verify_email', new=fake_verify_email)
    def test_activation_again_fail(self):
        url = reverse('users-list')
        data = {'username': 'TestUser', 'email': 'testuser@gmail.com', 'password': 'TestPassword'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        cache.delete('test')
        url = reverse('users-email')
        data = {'email': 'testuser@gmail.com'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        url = reverse('users-activate', kwargs={'uuid': '1111'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertFalse(User.objects.get(username='TestUser').email_verified)

