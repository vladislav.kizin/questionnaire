from rest_framework.mixins import CreateModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from django.core.cache import cache

from .models import User
from .serializers import UserSerializer


class UserViewSet(CreateModelMixin, GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [AllowAny]

    @action(methods=['GET'], detail=False, url_path=r'activate/(?P<uuid>.+)')
    def activate(self, request, uuid, *args, **kwargs):
        email = cache.get(uuid)
        if email:
            User.objects.filter(email=email).update(email_verified=True)
            return Response(200)
        raise ValidationError('No activation token for this email')

    @action(methods=['POST'], detail=False)
    def email(self, request, *args, **kwargs):
        email = request.data.get('email', None)
        if not email:
            return Response(400)
        user = User.objects.filter(email=email).first()
        if not user:
            return Response(400)
        user.verify_email()
        return Response(200)
