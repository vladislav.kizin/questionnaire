from unittest import mock
from django.urls import reverse, include, path
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.test import APIClient

from .models import Poll, TextFieldElement, SelectElement, MultipleSelectElement, RadioButtonElement,\
    IntegerPickerElement, CheckboxElement, IntegerResponse, CharResponse, BoolResponse, ArrayResponse
from .urls import router


USER_MODEL = get_user_model()

text_field = {
    "text_before": "Would you like to fill this TextField?",
    "text_after": "With some text after",
    "element_order": 1,
    "type": "TextField",
    "default_value": "Sure"
}

select_field = {
    "text_before": "Would you like to choose one thing from this Select?",
    "text_after": "With text here",
    "element_order": 2,
    "type": "Select",
    "all_values": [
        "First choice",
        "Second choice",
        "Third choice"
    ],
    "default_value": "First choice"
}

multiple_select_field = {
    "text_before": "Would you like to choose one or more things from this Select?",
    "text_after": "Surprise. Text also here",
    "element_order": 3,
    "type": "MultipleSelect",
    "all_values": [
        "First choice",
        "Second choice",
        "Third choice"
    ],
    "default_value": [
        "First choice",
        "Second choice"
    ]
}

radio_button_field = {
    "text_before": "Oh, look at this radio button",
    "text_after": "With pretty text after",
    "element_order": 4,
    "type": "RadioButton",
    "all_values": [
        "First choice",
        "Second choice",
        "Third choice"
    ],
    "default_value": "Second choice"
}

checkbox_field = {
    "text_before": "I will recommend this poll for my friends",
    "text_after": "",
    "element_order": 5,
    "type": "Checkbox",
    "default_value": False
}

integer_picker_field = {
    "text_before": "Choose your favorite number",
    "text_after": "Nice number",
    "element_order": 6,
    "type": "IntegerPicker",
    "default_value": 5,
    "min_value": 1,
    "max_value": 10
}

text_field_response = {
    "element_order": 1,
    "response": "Test"
}

select_field_response = {
    "element_order": 2,
    "response": "First choice"
}

multiple_select_response = {
    "element_order": 3,
    "response": ['First choice', 'Second choice']
}

radio_button_response = {
    "element_order": 4,
    "response": "First choice"
}

checkbox_response = {
    "element_order": 5,
    "response": True
}

integer_picker_response = {
    "element_order": 6,
    "response": 5
}

all_fields_response = [
    text_field_response,
    select_field_response,
    multiple_select_response,
    radio_button_response,
    checkbox_response,
    integer_picker_response
]

def fake_verify_email(self):
    pass


class PollTests(APITestCase, URLPatternsTestCase):
    urlpatterns = [
        path('api/', include(router.urls)),
        path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair')
    ]

    @mock.patch('users.models.User.verify_email', new=fake_verify_email)
    def setUp(self) -> None:
        USER_MODEL.objects.create(username='Test1', password=make_password('1q2w3e'), email='test@gmail.com')
        USER_MODEL.objects.create(username='Test2', password=make_password('1q2w3e'), email_verified=True,
                                  email='test2@gmail.com')
        self.token1 = ''
        self.token2 = ''
        self.login()

    def login(self):
        url = reverse('token_obtain_pair')
        data1 = {"username": "Test1", "password": "1q2w3e"}
        data2 = {"username": "Test2", "password": "1q2w3e"}
        response1 = self.client.post(url, data1, format='json')
        response2 = self.client.post(url, data2, format='json')
        self.token1 = f'Bearer {response1.data.get("access")}'
        self.token2 = f'Bearer {response2.data.get("access")}'

    def test_create_poll_with_text_field_access_all(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AA',
            'fields': [
                text_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(TextFieldElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_text_field_access_authorized(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AU',
            'fields': [
                text_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(TextFieldElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_text_field_access_verified(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'OV',
            'fields': [
                text_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(TextFieldElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_select_filed_access_all(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AA',
            'fields': [
                select_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(SelectElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_select_filed_access_authorized(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AU',
            'fields': [
                select_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(SelectElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_select_filed_access_verified(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'OV',
            'fields': [
                select_field
            ]
        }
        response = self.client.post(url, data, format='json')
        print(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(SelectElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_multiple_select_field_access_all(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AA',
            'fields': [
                multiple_select_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(MultipleSelectElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_multiple_select_field_access_authorized(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AU',
            'fields': [
                multiple_select_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(MultipleSelectElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_multiple_select_field_access_verified(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'OV',
            'fields': [
                multiple_select_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(MultipleSelectElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_radio_button_field_access_all(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AA',
            'fields': [
                radio_button_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(RadioButtonElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_radio_button_field_access_authorized(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AU',
            'fields': [
                radio_button_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(RadioButtonElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_radio_button_field_access_verified(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'OV',
            'fields': [
                radio_button_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(RadioButtonElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_checkbox_field_access_all(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AA',
            'fields': [
                checkbox_field
            ]}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CheckboxElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_checkbox_field_access_authorized(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AU',
            'fields': [
                checkbox_field
            ]}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CheckboxElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_checkbox_field_access_verified(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'OV',
            'fields': [
                checkbox_field
            ]}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CheckboxElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_integer_picker_field_access_all(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AA',
            'fields': [
                integer_picker_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(IntegerPickerElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_integer_picker_field_access_authorized(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AU',
            'fields': [
                integer_picker_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(IntegerPickerElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_integer_picker_field_access_verified(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'OV',
            'fields': [
                integer_picker_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(IntegerPickerElement.objects.count(), 1)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_all_fields_access_all(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AA',
            'fields': [
                text_field,
                select_field,
                multiple_select_field,
                radio_button_field,
                checkbox_field,
                integer_picker_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(TextFieldElement.objects.count(), 1)
        self.assertEqual(MultipleSelectElement.objects.count(), 1)
        self.assertEqual(RadioButtonElement.objects.count(), 1)
        self.assertEqual(CheckboxElement.objects.count(), 1)
        self.assertEqual(IntegerPickerElement.objects.count(), 1)
        self.assertEqual(SelectElement.objects.count(), 2)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_all_fields_access_authorized(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'AU',
            'fields': [
                text_field,
                select_field,
                multiple_select_field,
                radio_button_field,
                checkbox_field,
                integer_picker_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(TextFieldElement.objects.count(), 1)
        self.assertEqual(MultipleSelectElement.objects.count(), 1)
        self.assertEqual(RadioButtonElement.objects.count(), 1)
        self.assertEqual(CheckboxElement.objects.count(), 1)
        self.assertEqual(IntegerPickerElement.objects.count(), 1)
        self.assertEqual(SelectElement.objects.count(), 2)
        self.assertEqual(Poll.objects.count(), 1)

    def test_create_poll_with_all_fields_access_verified(self):
        url = reverse('polls-list')
        data = {
            'description': 'Test poll',
            'access_level': 'OV',
            'fields': [
                text_field,
                select_field,
                multiple_select_field,
                radio_button_field,
                checkbox_field,
                integer_picker_field
            ]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(TextFieldElement.objects.count(), 1)
        self.assertEqual(MultipleSelectElement.objects.count(), 1)
        self.assertEqual(RadioButtonElement.objects.count(), 1)
        self.assertEqual(CheckboxElement.objects.count(), 1)
        self.assertEqual(IntegerPickerElement.objects.count(), 1)
        self.assertEqual(SelectElement.objects.count(), 2)
        self.assertEqual(Poll.objects.count(), 1)

# RESPONSE FOR TEXT FIELD

    def test_response_for_text_field_access_all(self):
        self.test_create_poll_with_text_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [text_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 1)

    def test_response_for_text_field_access_not_str_response(self):
        self.test_create_poll_with_text_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url,
                                    [{"element_order": 1,"response": 12312}],
                                    format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

    def test_response_for_text_field_access_authorized(self):
        self.test_create_poll_with_text_field_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [text_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 1)

    def test_response_for_text_field_access_authorized_fail(self):
        self.test_create_poll_with_text_field_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [text_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

    def test_response_for_text_field_access_verified(self):
        self.test_create_poll_with_text_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token2)
        response = client.post(url, [text_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 1)

    def test_response_for_text_field_access_verified_without_activated_email(self):
        self.test_create_poll_with_text_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [text_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

    def test_response_for_text_field_access_verified_without_login(self):
        self.test_create_poll_with_text_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        response = client.post(url, [text_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

# RESPONSE FOR SELECT FIELD

    def test_response_for_select_field_access_all(self):
        self.test_create_poll_with_select_filed_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [select_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 1)

    def test_response_for_select_field_access_all_not_str_response(self):
        self.test_create_poll_with_select_filed_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url,
                                    [{"element_order": 2,"response": 123123}],
                                    format='json',
                                    HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

    def test_response_for_select_field_access_all_response_not_in_the_choices(self):
        self.test_create_poll_with_select_filed_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url,
                                    [{"element_order": 2, "response": "Fourth choice"}],
                                    format='json',
                                    HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

    def test_response_for_select_field_access_authorized(self):
        self.test_create_poll_with_select_filed_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [select_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 1)

    def test_response_for_select_field_access_authorized_fail(self):
        self.test_create_poll_with_select_filed_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [select_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

    def test_response_for_select_field_access_verified(self):
        self.test_create_poll_with_select_filed_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token2)
        response = client.post(url, [select_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 1)

    def test_response_for_select_field_access_verified_without_activated_email(self):
        self.test_create_poll_with_select_filed_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [select_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

    def test_response_for_select_field_access_verified_without_login(self):
        self.test_create_poll_with_select_filed_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        response = client.post(url, [select_field_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

# RESPONSE FOR MULTIPLE SELECT FIELD

    def test_response_for_multiple_select_field_access_all(self):
        self.test_create_poll_with_multiple_select_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [multiple_select_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ArrayResponse.objects.count(), 1)

    def test_response_for_multiple_select_field_access_all_not_all_responses_in_choices(self):
        self.test_create_poll_with_multiple_select_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url,
                                    [{"element_order": 3, "response":["Third choice", "Fourth choice"]}],
                                    format='json',
                                    HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ArrayResponse.objects.count(), 0)

    def test_response_for_multiple_select_field_access_authorized(self):
        self.test_create_poll_with_multiple_select_field_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [multiple_select_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ArrayResponse.objects.count(), 1)

    def test_response_for_multiple_select_field_access_authorized_fail(self):
        self.test_create_poll_with_multiple_select_field_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [multiple_select_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ArrayResponse.objects.count(), 0)

    def test_response_for_multiple_select_field_access_verified(self):
        self.test_create_poll_with_multiple_select_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token2)
        response = client.post(url, [multiple_select_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ArrayResponse.objects.count(), 1)

    def test_response_for_multiple_select_field_access_verified_without_activated_email(self):
        self.test_create_poll_with_multiple_select_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [multiple_select_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ArrayResponse.objects.count(), 0)

    def test_response_for_multiple_select_field_access_verified_without_login(self):
        self.test_create_poll_with_multiple_select_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        response = client.post(url, [multiple_select_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ArrayResponse.objects.count(), 0)

# RESPONSE FOR RADIO BUTTON FIELD

    def test_response_for_radio_button_field_access_all(self):
        self.test_create_poll_with_radio_button_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [radio_button_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 1)

    def test_response_for_radio_button_field_access_all_wrong_response_type(self):
        self.test_create_poll_with_radio_button_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url,
                                    [{"element_order": 4, "response": 123131}],
                                    format='json',
                                    HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

    def test_response_for_radio_button_field_access_authorized(self):
        self.test_create_poll_with_radio_button_field_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [radio_button_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 1)

    def test_response_for_radio_button_field_access_authorized_fail(self):
        self.test_create_poll_with_radio_button_field_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [radio_button_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

    def test_response_for_radio_button_field_access_verified(self):
        self.test_create_poll_with_radio_button_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token2)
        response = client.post(url, [radio_button_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 1)

    def test_response_for_radio_button_field_access_verified_without_activated_email(self):
        self.test_create_poll_with_radio_button_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [radio_button_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

    def test_response_for_radio_button_field_access_verified_without_login(self):
        self.test_create_poll_with_radio_button_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        response = client.post(url, [radio_button_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)

# RESPONSE FOR CHECKBOX FIELD

    def test_response_for_checkbox_field_access_all(self):
        self.test_create_poll_with_checkbox_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [checkbox_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BoolResponse.objects.count(), 1)

    def test_response_for_checkbox_field_access_all_wrong_response_type(self):
        self.test_create_poll_with_checkbox_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url,
                                    [{"element_order": 5, "response": 'True'}],
                                    format='json',
                                    HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BoolResponse.objects.count(), 0)

    def test_response_for_checkbox_field_access_authorized(self):
        self.test_create_poll_with_checkbox_field_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [checkbox_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BoolResponse.objects.count(), 1)

    def test_response_for_checkbox_field_access_authorized_fail(self):
        self.test_create_poll_with_checkbox_field_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [checkbox_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BoolResponse.objects.count(), 0)

    def test_response_for_checkbox_field_access_verified(self):
        self.test_create_poll_with_checkbox_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token2)
        response = client.post(url, [checkbox_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BoolResponse.objects.count(), 1)

    def test_response_for_checkbox_field_access_verified_without_activated_email(self):
        self.test_create_poll_with_checkbox_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [checkbox_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BoolResponse.objects.count(), 0)

    def test_response_for_checkbox_field_access_verified_without_login(self):
        self.test_create_poll_with_checkbox_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        response = client.post(url, [checkbox_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BoolResponse.objects.count(), 0)

# RESPONSE FOR INTEGER PICKER FIELD

    def test_response_for_integer_picker_field_access_all(self):
        self.test_create_poll_with_integer_picker_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [integer_picker_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(IntegerResponse.objects.count(), 1)

    def test_response_for_integer_picker_field_access_all_wrong_response_type(self):
        self.test_create_poll_with_integer_picker_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url,
                                    [{"element_order": 6, "response": '5'}],
                                    format='json',
                                    HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(IntegerResponse.objects.count(), 0)

    def test_response_for_integer_picker_field_access_all_response_not_in_field_range(self):
        self.test_create_poll_with_integer_picker_field_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url,
                                    [{"element_order": 6, "response": 11}],
                                    format='json',
                                    HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(IntegerResponse.objects.count(), 0)

    def test_response_for_integer_picker_field_access_authorized(self):
        self.test_create_poll_with_integer_picker_field_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [integer_picker_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(IntegerResponse.objects.count(), 1)

    def test_response_for_integer_picker_field_access_authorized_fail(self):
        self.test_create_poll_with_integer_picker_field_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, [integer_picker_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(IntegerResponse.objects.count(), 0)

    def test_response_for_integer_picker_field_access_verified(self):
        self.test_create_poll_with_integer_picker_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token2)
        response = client.post(url, [integer_picker_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(IntegerResponse.objects.count(), 1)

    def test_response_for_integer_picker_field_access_verified_without_activated_email(self):
        self.test_create_poll_with_integer_picker_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, [integer_picker_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(IntegerResponse.objects.count(), 0)

    def test_response_for_integer_picker_field_access_verified_without_login(self):
        self.test_create_poll_with_integer_picker_field_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        response = client.post(url, [integer_picker_response], format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(IntegerResponse.objects.count(), 0)

# RESPONSE FOR ALL FIELDS

    def test_response_for_all_fields_access_all(self):
        self.test_create_poll_with_all_fields_access_all()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, all_fields_response, format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 3)
        self.assertEqual(IntegerResponse.objects.count(), 1)
        self.assertEqual(BoolResponse.objects.count(), 1)
        self.assertEqual(ArrayResponse.objects.count(), 1)

    def test_response_for_all_fields_access_authorized(self):
        self.test_create_poll_with_all_fields_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, all_fields_response, format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 3)
        self.assertEqual(IntegerResponse.objects.count(), 1)
        self.assertEqual(BoolResponse.objects.count(), 1)
        self.assertEqual(ArrayResponse.objects.count(), 1)

    def test_response_for_all_fields_access_authorized_fail(self):
        self.test_create_poll_with_all_fields_access_authorized()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        response = self.client.post(url, all_fields_response, format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)
        self.assertEqual(IntegerResponse.objects.count(), 0)
        self.assertEqual(BoolResponse.objects.count(), 0)
        self.assertEqual(ArrayResponse.objects.count(), 0)

    def test_response_for_all_fields_access_verified(self):
        self.test_create_poll_with_all_fields_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token2)
        response = client.post(url, all_fields_response, format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CharResponse.objects.count(), 3)
        self.assertEqual(IntegerResponse.objects.count(), 1)
        self.assertEqual(BoolResponse.objects.count(), 1)
        self.assertEqual(ArrayResponse.objects.count(), 1)

    def test_response_for_all_fields_access_verified_without_activated_email(self):
        self.test_create_poll_with_all_fields_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.token1)
        response = client.post(url, all_fields_response, format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)
        self.assertEqual(IntegerResponse.objects.count(), 0)
        self.assertEqual(BoolResponse.objects.count(), 0)
        self.assertEqual(ArrayResponse.objects.count(), 0)

    def test_response_for_all_fields_access_verified_without_login(self):
        self.test_create_poll_with_all_fields_access_verified()
        poll = Poll.objects.first()
        url = reverse('polls-vote', kwargs={'uuid': poll.uuid})
        client = APIClient()
        response = client.post(url, all_fields_response, format='json', HTTP_USER_AGENT='Test')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CharResponse.objects.count(), 0)
        self.assertEqual(IntegerResponse.objects.count(), 0)
        self.assertEqual(BoolResponse.objects.count(), 0)
        self.assertEqual(ArrayResponse.objects.count(), 0)
